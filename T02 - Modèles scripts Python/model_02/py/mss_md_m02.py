#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Developing adaptative computational models for research-oriented outputs to mss metadata.

MODEL N°2: CHRONOLOGICAL DISTRIBUTION

INPUT DATA: TEI
OUTPUT DATA: CSV

Pulls out the mss titles and origin dates from TEI metadata files,
and converts the origin dates into standard datetime format (%m-%d-%y %H:%M:%S)

Outputs the mss title, origin date not-before and origin date non-after.

Created on February 2020.
"""

#-------------------------------------------------------------------------------------------

# The BeautifulSoup library parses the element trees of the XML input files
from bs4 import BeautifulSoup

# The re module provides regular expressions matching operations
import re

import os

# The glob module generates lists of files matching given patterns (file paths)
import glob

# the datetime module allows working with dates, times and time intervals
from datetime import datetime

#-------------------------------------------------------------------------------------------

__author__ = "Diandra Cristache"
__copyright__ = " "
__credits__ = ["Cambridge Digital Library"]
__license__ = "Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) "
__maintainer__ = "Diandra Cristache"
__email__ = "diandra.cristache@etu.univ-tours.fr"
__status__ = "Production"

#-------------------------------------------------------------------------------------------

# Set a CSV delimiter 
# (either comma or semi-colon, according to the requirement of the output tool)
# and store it in the variable 'delimiter'
delimiter = ","
                    
# Print the header row in the output file
# (separate the fields with a delimiter)
print("TITLE",delimiter,"DATE",delimiter,"MIN DATE",delimiter,"MAX DATE",file=open("../csv/mss_md_m02.csv", "w"))

# Select the path of the corpus to parse
# and store it in the variable 'corpus'
corpus = glob.glob('../xml/*/*.xml')

# Parse each file in the corpus
for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
        
        # Find all the occurrences of the element <title> 
        # (only when sub-elements of <titleStmt>)
        # and store the results in the list 'mssTitle'
        mssTitle = soup.select('titleStmt title')
        
        # For each element <title> in the list
        for a in mssTitle:
            
            # Get the content of the element <title>
            # Store the content in the variable 'title'
            # and strip all the blank spaces and new lines
            title = ' '.join(a.get_text().split())
                
        # ----------------------------------------------------
        # ---------------- PULL OUT THE DATE ----------------
        # ----------------------------------------------------
        
        mssOrigDate = soup.find_all('origDate')
        
        for a in mssOrigDate:

            # ----------------DATE ----------------
                
            try:
                when = a['when']
            except KeyError:
                when = None
                
            if when:
                when = re.sub('-.*$', '', when)
            
            # ----------------MINIMUM DATE ----------------
                
            try: 
                minDate = a['notBefore']
            except KeyError:
                minDate = None
                
            if minDate:
                minDate = re.sub('-.*$', '', minDate)
                
            if minDate:
                minDate = re.sub('-.*$', '', minDate)
                
            # --------------- MAXIMUM DATE ----------------
            
            try: 
                maxDate = a['notAfter']
            except KeyError:
                maxDate = None
                
            if maxDate:
                maxDate = re.sub('-.*$', '', maxDate)
                
    
            # (For each file in the corpus) print the extracted information in a row
            # and write each row in the CSV output file
            print(''.join(title),delimiter,when,delimiter,minDate,delimiter,maxDate,file=open("../csv/mss_md_m02.csv", "a"))

