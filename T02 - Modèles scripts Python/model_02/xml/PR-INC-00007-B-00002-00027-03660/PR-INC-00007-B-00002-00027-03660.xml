<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>Inc.7.B.2.27[3660]</title>
         </titleStmt>
         <publicationStmt>
            <date calendar="Gregorian">2018-08-22</date>
            <publisher>Cambridge University Library</publisher>
            <availability xml:id="displayImageRights" status="restricted">
               <p>Zooming image © Cambridge University Library, All rights reserved.</p>
            </availability>
            <availability xml:id="downloadImageRights" status="restricted">
               <licence>Images made available for download are licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License (CC-BY-NC 3.0)</licence>
            </availability>
            <availability xml:id="metadataRights" status="restricted">
               <licence>This metadata is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.</licence>
            </availability>
         </publicationStmt>
         <sourceDesc>
            <msDesc xml:id="n1" xml:lang="eng">
               <msIdentifier>
                  <repository>Cambridge University Library</repository>
                  <idno>Inc.7.B.2.27[3660]</idno>
               </msIdentifier>
               <msContents>
                  <summary>
                     <seg type="para">The tiny town of Loreto in the Marche region of Italy was firmly established as a place of pilgrimage by the fifteenth century. Pilgrims came to visit the ‘santa casa’, the holy house of the Virgin Mary, brought from Palestine by angels in the thirteenth century, according to legend, and eventually deposited on this out-of-the-way hilltop. This brief printed pamphlet is a rare example of the kind of pilgrim souvenir that a visitor to the site might acquire for a small sum from a local vendor. As well as retelling the history of the holy house, the pamphlet could act as a form of “mental pilgrimage”, to be brought home for those who could not visit the site in person. The ownership inscription on the frontispiece in an early sixteenth-century hand helps to make clear the value of this kind of item to its owner.</seg> 
                     <seg type="para">Dr Abigail Brundin</seg> 
                     <seg type="para">This item is part of the <ref target="http://www.fitzmuseum.cam.ac.uk/madonnasandmiracles">Madonnas &amp; Miracles exhibition</ref> at the Fitzwilliam Museum, 7th March-4th June 2017</seg>
                  </summary>
                  <msItem>
                     <title>Translatio miraculosa ecclesie beate Marie uirginis de Loreto</title>
                     <title type="alt">Legenda translationis miraculosae ecclesiae B.M.V. de Loreto</title>
                     <textLang mainLang="lat">Latin</textLang>
                     <respStmt>
                        <resp/>
                        <name type="person" role="printer">
                           <persName type="standard">Silber, Eucharius, active 1480-1510</persName>
                           <persName type="display">Silber, Eucharius, active 1480-1510</persName>
                        </name>
                        <name type="person" role="fmo">
                           <persName type="standard">Novellus, Petrus, active 16th century</persName>
                           <persName type="display">Novellus, Petrus, active 16th century</persName>
                        </name>
                        <name type="person" role="fmo">
                           <persName type="standard">Parmius, Augustinus, active 16th century</persName>
                           <persName type="display">Parmius, Augustinus, active 16th century</persName>
                        </name>
                        <name type="person" role="fmo">
                           <persName type="standard">De Luca, Tommaso, 1752-1829</persName>
                           <persName type="display">De Luca, Tommaso, 1752-1829</persName>
                        </name>
                        <name type="person" role="dnr">
                           <persName type="standard">Murray, Charles Fairfax, 1849-1919</persName>
                           <persName type="display">Murray, Charles Fairfax, 1849-1919</persName>
                        </name>
                        <name type="person" role="oth">
                           <persName type="standard">Teramano, Pietro.</persName>
                           <persName type="display">Teramano, Pietro.</persName>
                        </name>
                        <name type="corporation" role="pbl">
                           <persName type="standard">Eucharius Silber</persName>
                           <persName type="display">Eucharius Silber</persName>
                        </name>
                     </respStmt>
                     <note>Title from title page on leaf [a1] recto. The author is Petrus Georgii Tolomei (Teramanus) Full-page woodcut illustration of the church of St Mary at Loreto on leaf [a1] recto, from another Eucharius Silber's edition (ISTC it00426300; Sander 4287, pl. 762) Woodcut initial on leaf [a2] recto. 23 lines to the page. Signatures: [a⁴]</note>
                  </msItem>
               </msContents>
               <physDesc>
                  <objectDesc>
                     <supportDesc material="paper">
                        <support>Paper</support>
                        <extent>[4] leaves : ill., init. (woodcuts) ; 130 mm. (8vo)</extent>
                     </supportDesc>
                  </objectDesc>
               </physDesc>
               <history>
                  <origin>
                     <origDate calendar="Gregorian" notBefore="1501" notAfter="1501">after 1500?</origDate>
                     <origPlace ref="7000874">Rome</origPlace>
                  </origin>
                  <provenance>Inscribed "Ex libris pr[es]b[ite]ri Petri Nouelli Par[men]sis [?]" on leaf [a1] recto, Italy, early 16th century. Inscribed "Huius libri scripsit Augustinus Parmius posessor [sic]" on leaf [a4] verso, 16th century. Possibly Don Tommaso De Luca, bibliographer and collector of manuscripts, incunabula and rare books: see Inc.7.B.2.27[4242], with which it was formerly bound. Charles Fairfax Murray, artist and art connoisseur, his number "2220" and bibliographical note in pencil on verso of first upper free endpaper; given by him on 16 June 1918.</provenance>
               </history>
               <additional>
                  <adminInfo/>
               </additional>
            </msDesc>
         </sourceDesc>
      </fileDesc>
      <encodingDesc>
         <classDecl>
            <taxonomy xml:id="LCSH">
               <bibl>
                  <ref target="http://id.loc.gov/authorities/about.html#lcsh">Library
                                    of Congress Subject Headings</ref>
               </bibl>
            </taxonomy>
         </classDecl>
      </encodingDesc>
      <profileDesc>
         <textClass>
            <keywords scheme="#LCSH">
               <list>
                  <item>
                     <ref>Mary, Blessed Virgin, Saint -- Devotion to -- Italy</ref>
                  </item>
                  <item>
                     <ref>Mary, Blessed Virgin, Saint -- Early works to 1800</ref>
                  </item>
                  <item>
                     <ref>Italy -- Rome</ref>
                  </item>
               </list>
            </keywords>
         </textClass>
      </profileDesc>
   </teiHeader>
   <facsimile>
      <graphic decls="#document-thumbnail"
               rend="portrait"
               url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00001_files/8/0_0.jpg"/>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="1" xml:id="i1">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00001.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00001_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="2" xml:id="i2">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00002.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00002_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="3" xml:id="i3">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00003.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00003_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="4" xml:id="i4">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00004.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00004_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="5" xml:id="i5">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00005.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00005_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="6" xml:id="i6">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00006.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00006_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="7" xml:id="i7">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00007.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00007_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="8" xml:id="i8">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00008.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00008_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="9" xml:id="i9">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00009.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00009_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="10" xml:id="i10">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00010.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00010_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="11" xml:id="i11">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00011.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00011_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="12" xml:id="i12">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00012.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00012_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="13" xml:id="i13">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00013.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00013_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="14" xml:id="i14">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00014.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00014_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="15" xml:id="i15">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00015.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00015_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="16" xml:id="i16">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00016.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00016_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="17" xml:id="i17">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00017.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00017_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="18" xml:id="i18">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00018.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00018_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="19" xml:id="i19">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00019.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00019_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="20" xml:id="i20">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00020.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00020_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="21" xml:id="i21">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00021.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00021_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
      <surface xmlns="http://www.tei-c.org/ns/1.0" n="22" xml:id="i22">
         <graphic 
                  decls="#downloadImageRights #download"
                  height="3590px"
                  width="2583px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00022.jpg"/>
         <graphic 
                  decls="#thumbnail"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00007-B-00002-00027-03660-000-00022_files/8/0_0.jpg"
                  rend="portrait"/>
      </surface>
   </facsimile>
   <text>
      <body>
         <div>
            <pb xml:id="Lg-1" n="1" facs="#i1"/>
            <pb xml:id="Lg-2" n="2" facs="#i2"/>
            <pb xml:id="Lg-3" n="3" facs="#i3"/>
            <pb xml:id="Lg-4" n="4" facs="#i4"/>
            <pb xml:id="Lg-5" n="5" facs="#i5"/>
            <pb xml:id="Lg-6" n="6" facs="#i6"/>
            <pb xml:id="Lg-7" n="7" facs="#i7"/>
            <pb xml:id="Lg-8" n="8" facs="#i8"/>
            <pb xml:id="Lg-9" n="9" facs="#i9"/>
            <pb xml:id="Lg-10" n="10" facs="#i10"/>
            <pb xml:id="Lg-11" n="11" facs="#i11"/>
            <pb xml:id="Lg-12" n="12" facs="#i12"/>
            <pb xml:id="Lg-13" n="13" facs="#i13"/>
            <pb xml:id="Lg-14" n="14" facs="#i14"/>
            <pb xml:id="Lg-15" n="15" facs="#i15"/>
            <pb xml:id="Lg-16" n="16" facs="#i16"/>
            <pb xml:id="Lg-17" n="17" facs="#i17"/>
            <pb xml:id="Lg-18" n="18" facs="#i18"/>
            <pb xml:id="Lg-19" n="19" facs="#i19"/>
            <pb xml:id="Lg-20" n="20" facs="#i20"/>
            <pb xml:id="Lg-21" n="21" facs="#i21"/>
            <pb xml:id="Lg-22" n="22" facs="#i22"/>
         </div>
      </body>
   </text>
</TEI>
