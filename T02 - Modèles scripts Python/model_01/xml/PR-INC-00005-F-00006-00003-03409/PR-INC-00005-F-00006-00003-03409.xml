<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>Inc.5.F.6.3[3409]</title>
            <funder/>
         </titleStmt>
         <publicationStmt>
            <date calendar="Gregorian">2018-04-25</date>
            <publisher>Cambridge University Library</publisher>
            <pubPlace>
               <address>
                  <addrLine>Cambridge University Library</addrLine>
                  <street>West Road</street>
                  <settlement>Cambridge</settlement>
                  <postCode>CB3 9DR</postCode>
                  <addrLine>
                     <ref target="http://www.lib.cam.ac.uk">Cambridge University Library</ref>
                  </addrLine>
                  <addrLine>
                     <email>library@lib.cam.ac.uk</email>
                  </addrLine>
               </address>
            </pubPlace>
            <availability xml:id="displayImageRights" status="restricted">
               <p>Zooming image © Cambridge University Library, All rights reserved.</p>
            </availability>
            <availability xml:id="downloadImageRights" status="restricted">
               <licence>Images made available for download are licensed under a Creative Commons
                  Attribution-NonCommercial 3.0 Unported License (CC BY-NC 3.0)</licence>
            </availability>
            <availability xml:id="metadataRights" status="restricted">
               <licence>This metadata is licensed under a Creative Commons Attribution-NonCommercial
                  3.0 Unported License.</licence>
            </availability>
         </publicationStmt>
         <sourceDesc>
            <msDesc xml:id="n1" xml:lang="eng">
               <msIdentifier>
                  <institution>Cambridge University</institution>
                  <repository>Cambridge University Library</repository>
                  <idno>Inc.5.F.6.3[3409]</idno>
               </msIdentifier>
               <msContents>
                  <summary>
                     <seg type="para">This ghostly impression is the only surviving evidence of the printing of an indulgence issued by William Caxton. This is not a true impression, but rather a trial print on the last page of an entirely unrelated book. The woodcut was set with letterpress, then the whole was daubed with a brown substance, and a trial impression taken on a blank leaf of a book printed in Antwerp, presumably lying around in Caxton’s printing house awaiting sale. Multiple impressions would subsequently have been taken using printer’s ink and clean paper, but none of these survive.</seg>
                  </summary>
                  <msItem>
                     <title>[Image of pity]</title>
                     <title type="alt">Indulgence (Image of pity)</title>
                     <title type="alt">To them that before this ymage of pyte deuowtely say
                        .v. Pr̃ nr̃</title>
                     <title type="alt">To them that before this ymage of pyte devowtely say
                        V Pater noster</title>
                     <title/>
                     <textLang mainLang="eng"/>
                     <author>
                        <name type="person" role="aut">
                           <persName type="standard">Catholic Church</persName>
                           <persName type="display">Catholic Church</persName>
                        </name>
                     </author>
                     <respStmt>
                        <resp/>
                        <name type="person" role="prt">
                           <persName type="standard">Caxton, William approximately 1422-1491 or
                              1492</persName>
                           <persName type="display">Caxton, William approximately 1422-1491 or
                              1492</persName>
                        </name>
                        <name type="corporation" role="pbl">
                           <persName type="standard">William Caxton</persName>
                           <persName type="display">William Caxton</persName>
                        </name>
                     </respStmt>
                     <note>Depicts Christ with the Cross behind, flanked by the instruments of the
                        passion in 18 compartments.</note>
                     <note>Text reads (transcription from Bradshaw): To them that before this ymage
                        of pyte deuowtely say .v. Pr̃ nr̃ v. Aueys &amp; a Credo. pyteously
                        beholdyng these armes of xp̄s passiō ar- graūted xxij. M. vij.C &amp; .lv.
                        yeres of pardon.</note>
                     <note>Survives in a trial impression on final blank page of: Jacobus de
                        Gruytrode. Colloquium peccatoris et crucifixi Jesu Christi. [Antwerp :
                        Mathias van der Goes, between 14 Feb. 1487 and 21 May 1490] (Oates
                        3948).</note>
                  </msItem>
               </msContents>
               <physDesc>
                  <objectDesc>
                     <supportDesc material="paper">
                        <support>Paper</support>
                        <extent>1 woodcut : chiefly ill. ; 140 x 92 mm (1/4 sheet 4to.)</extent>
                     </supportDesc>
                  </objectDesc>
               </physDesc>
               <history>
                  <origin>
                     <origDate calendar="Gregorian" notBefore="1490" notAfter="1490">ca. 1490]</origDate>
                     <origPlace ref="7018934">Westminster</origPlace>
                  </origin>
                  <provenance/>
               </history>
               <additional>
                  <adminInfo>
                     <availability status="restricted">
                        <p>Entry to read in the Library is permitted only on presentation of a valid
                           reader's card for admissions procedures contact <ref target="http://www.lib.cam.ac.uk/cgi-bin/eligibility.cgi/">Cambridge
                              University Library Admissions</ref>).</p>
                     </availability>
                     <note/>
                  </adminInfo>
                  <listBibl>
                     <bibl>Blake, N.F. William Caxton: a bibliographical guide, B100</bibl>
                     <bibl>Bradshaw, H. "On the earliest English engravings of the indulgence known as the 'Image of Pity'" Collected papers, p. 84-100</bibl>
                     <bibl>De Ricci, S. A census of Caxtons, 55</bibl>
                     <bibl>ESTC, S93200</bibl>
                     <bibl>Hodnett, E. English woodcuts, 380</bibl>
                     <bibl>Oates, 4112</bibl>
                     <bibl>STC (2nd ed.), 14077c.8</bibl>
                  </listBibl>
               </additional>
            </msDesc>
         </sourceDesc>
      </fileDesc>
      <encodingDesc>
         <classDecl>
            <taxonomy xml:id="LCSH">
               <bibl>
                  <ref target="http://id.loc.gov/authorities/about.html#lcsh">Library of Congress
                     Subject Headings</ref>
               </bibl>
            </taxonomy>
         </classDecl>
      </encodingDesc>
      <profileDesc>
         <textClass>
            <keywords scheme="#LCSH">
               <list>
                  <item>
                     <ref>Jesus Christ -- -- Passion -- Art</ref>
                  </item>
                  <item>
                     <ref>Indulgences</ref>
                  </item>
               </list>
            </keywords>
         </textClass>
      </profileDesc>
      <revisionDesc>
         <change when="2012-04-10">
            <persName>CUL</persName>
         </change>
      </revisionDesc>
   </teiHeader>
   <facsimile>
      <graphic decls="#document-thumbnail"
               rend="portrait"
               url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00005-F-00006-00003-03409-000-00001_files/8/0_0.jpg"/>
      <surface n="1" xml:id="i1">
         <graphic decls="#downloadImageRights #download"
                  height="9337px"
                  width="6798px"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00005-F-00006-00003-03409-000-00001.jpg"/>
         <graphic decls="#thumbnail"
                  rend="portrait"
                  url="http://cudl.lib.cam.ac.uk/content/images/PR-INC-00005-F-00006-00003-03409-000-00001_files/8/0_0.jpg"/>
      </surface>
   </facsimile>
   <text>
      <body>
         <div>
            <pb n="1" xml:id="pb-1" facs="#i1"/>
         </div>
      </body>
   </text>
   
</TEI>
