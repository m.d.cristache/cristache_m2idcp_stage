#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Developing adaptative computational models for research-oriented outputs to mss metadata.

MODEL N°1 : GEOGRAPHIC DISTRIBUTION W/ URIs OF TEXT & IMAGE METADATA 

INPUT DATA: TEI
OUTPUT DATA: CSV

Extracts the names and identifiers of origin places in TEI metadata files, 
retrieves their latitude and longitude from the matching Getty Geonames rdf description, 
retrieves the URI of the matching XML metadata file & IIIF manifest.

Outputs the mss title, origin place name, origin place latitude & longitude, URI of matching TEI metadata,
URI of matching IIIF description.

The results are output in CSV format and suit the format requirements of Google Earth Pro. 

Created on February 2020.
"""

#-------------------------------------------------------------------------------------------

# The BeautifulSoup library parses the element trees of the XML input files
from bs4 import BeautifulSoup

# The glob module generates lists of files matching given patterns (file paths)
import glob

import os

# The rdflib library allows working with RDF
import rdflib

import requests

#-------------------------------------------------------------------------------------------

__author__ = "Diandra Cristache"
__copyright__ = " "
__credits__ = ["Cambridge Digital Library"]
__license__ = "Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) "
__maintainer__ = "Diandra Cristache"
__email__ = "diandra.cristache@etu.univ-tours.fr"
__status__ = "Production"

#-------------------------------------------------------------------------------------------

# Set the CSV delimiter (according to the requirement of the output tool)
# and store it in the variable 'delimiter'
delimiter = ","
                    
# Set the header row (separate the fields with a delimiter)
# Print the header row in the output CSV file
print("TITLE",delimiter,"PLACE NAME",delimiter,"LAT",delimiter,"LON",delimiter,"TEI METADATA",delimiter,"IIIF METADATA", file=open("../csv/mss_md_m01.csv", "w"))

# Select the path of the corpus to parse
# and store it in the variable 'corpus'
corpus = glob.glob('../xml/*/*.xml')

# Parse each file in the corpus
for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
        
        # Find all the occurrences of the element <title> 
        # (only when sub-elements of <titleStmt>)
        # and store the results in the list 'mssTitle'
        mssTitle = soup.select('titleStmt title')

        # For each element <title> in the list
        for a in mssTitle:

            # Get the content of the element <title>
            # Store the content in the variable 'title'
            # and strip the useless blank spaces and new lines
            title = a.get_text().split()

        # ----------------------------------------------------
        # ---------------- LINKS TO TEI METADATA -------------
        # ----------------------------------------------------
        
        try:
            uriTEI = "https://services.cudl.lib.cam.ac.uk/v1/metadata/tei/{}".format(filename)
            request = requests.get(uriTEI)
            if request.status_code == 200: 
                uriTEI = uriTEI
            else:
                uriTEI = None
        except FileNotFoundError:
            uriTEI= None
        
        # ----------------------------------------------------
        # ---------------- LINKS TO IIIF METADATA ------------
        # ----------------------------------------------------
            
        try:
            uriIIIF = "https://cudl.lib.cam.ac.uk/iiif/{}".format(filename)
            request = requests.get(uriIIIF)
            if request.status_code == 200:
                uriIIIF = uriIIIF
            else:
                uriIIIF = None
        except FileNotFoundError:
            uriIIIF= None
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PLACE ----------------
        # ----------------------------------------------------
        
        mssPlace = soup.find('origPlace')
            
        if mssPlace:
            
            for a in mssPlace:
                placeName = ' '.join(mssPlace.get_text().replace(',', ' -').split())
                
                try:
                    placeKey = str(mssPlace['ref'])
                except KeyError:
                    placeKey = None
                
                if placeKey is not None:
                
                    try:
                        
                        uri = "http://vocab.getty.edu/page/tgn/{}.rdf".format(placeKey)
                        request = requests.get(uri)
                        
                        if request.status_code == 200:
                            g = rdflib.Graph()
                            parse = g.parse(uri)
            
                            # ---------------- PULL OUT THE COORDINATES ----------------
            
                            ask = parse.query(
                                    """ASK {?x schema:latitude ?latitude .
                                            ?x schema:longitude ?longitude .
                                    }""")
        
                            if ask:
                                
                                # ---------------- PULL OUT THE LATITUDE ----------------
                                
                                qres_lat = parse.query(
                                    """SELECT ?latitude
                                       WHERE {
                                          ?x schema:latitude ?latitude .
                                       }""")
                                for row in qres_lat:
                                    row_lat = "%s" %row
                
                                # ---------------- PULL OUT THE LONGITUDE ----------------
                
                                qres_lon = parse.query(
                                    """SELECT ?longitude
                                       WHERE {
                                          ?x schema:longitude ?longitude .
                                       }""")
                                for row in qres_lon:
                                    row_lon =  "%s" %row
                                
                            else:
                                row_lat = None
                                row_lon = None
                            
                        else:
                            row_lat = None
                            row_lon = None
                            
                    except requests.ConnectionError as e:
                        print("Debug exception : ",e)
                        print("url: ",uri)
                        print("code: ",request.status_code)
                        row_lat = None
                        row_lon = None
                    
                    # ----------------------------------------------------
                    # ---------------- PRINT RESULTS ----------------
                    # ----------------------------------------------------
                    if row_lat and row_lon is not None:
                
                        # For each file in the corpus, print the extracted information in a row
                        # and append the row in the unique CSV output file
                        print(' '.join(title),delimiter,' '.join(placeName),delimiter,row_lat,delimiter,row_lon, delimiter,uriTEI,delimiter,uriIIIF,file=open("../csv/mss_md_m01.csv", "a"))


