#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Developing adaptative computational models for research-oriented outputs to mss metadata.

MODEL N°4: METADATA FROM SOURCE CODE OF ONLINE RESOURCES

INPUT DATA: TEI, HTML
OUTPUT DATA: CSV

Pulls out the mss titles from local TEI metadata files, 
pulls out the URI of the related PINAKES notice, parses the source code of the PINAKES notice,
retrieves the URIs and names of authors of related items.

Outputs the mss title and the names of the authors of related items.

Created on February 2020.
"""

#-------------------------------------------------------------------------------------------

# The BeautifulSoup library parses the element trees of the XML input files
from bs4 import BeautifulSoup

# The re module provides regular expressions matching operations
import re

# The glob module generates lists of files matching given patterns (file paths)
import glob

# The os module provides a way of using operating system dependent functionality
import os

# The urllib module allows interactions with URLs
from urllib.request import urlopen

# The rdflib library allows interactions with RDF
import rdflib

#-------------------------------------------------------------------------------------------

__author__ = "Diandra Cristache"
__copyright__ = " "
__credits__ = ["Cambridge Digital Library"]
__license__ = "Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) "
__maintainer__ = "Diandra Cristache"
__email__ = "diandra.cristache@etu.univ-tours.fr"
__status__ = "Production"

#-------------------------------------------------------------------------------------------

# Set a CSV delimiter 
# (either comma or semi-colon, according to the requirement of the output tool)
# and store it in the variable 'delimiter'
delimiter = ";"
                    
# Print the header row in the output file
# (separate the fields with a delimiter)
print("MSS TITLE",delimiter,"RELATED CONTRIBUTORS",delimiter,"ORIGIN PLACE",file=open("../csv/mss_md_m03.csv", "w"))

# Select the path of the corpus to parse
# and store it in the variable 'corpus'
corpus = glob.glob('../xml/Bodleian_Barocci/*.xml')

# Call the rdflib library
g = rdflib.Graph()

# Parse each file in the corpus
for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
    
    # Pull out the name of current file
    # from the local file path
    filename=os.path.splitext(os.path.basename(file))[0]
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # ----------------------------------------------------
        # ---------------- PULL OUT THE TITLE ----------------
        # ----------------------------------------------------
                
        # Find all the occurrences of the element <title> 
        # (only when sub-elements of <titleStmt>)
        # and store the results in the list 'mssTitle'
        mssTitle = soup.select('titleStmt title')
        
        # For each element <title> in the list
        for a in mssTitle:
            
            title = mssTitle[0]

            # Get the content of the element <title>
            # Store the content in the variable 'title'
            # and strip all the blank spaces and new lines
            title = title.get_text().split()
            
        # ----------------------------------------------------
        # ---------------- PULL OUT THE PLACE ----------------
        # ----------------------------------------------------
        
        mssPlace = soup.find('origPlace')
            
        if mssPlace:
            
            for a in mssPlace:
                placeName = ' '.join(mssPlace.get_text().replace(',', ' -').split())
                
        else:
            placeName = None
                
         # ----------------------------------------------------
         # ---------------- LINK TO PINAKES NOTICE -------------
         # ----------------------------------------------------

        # Find all the occurrences of the element <ref> in the element <bibl>
        # Store the content in the variable 'refList'
        refList = soup.select('bibl ref')

        # For each element <ref> in the list
        for a in refList:
            
            # Pull out the value of the attribute @target
            # and store it the the variable 'refkey'
            refkey = str(a['target'])
            
            response = urlopen(refkey)
            
            page_source = response.read()

            # Give BeautifulSoup access to the file content
            # and store the access in the variable 'soup'
            source = BeautifulSoup(page_source, "html")
            
            # ---------------- PULL OUT THE AUTHOR'S URI AND NAME ----------------
            
            uri_auth = re.compile(r"^/notices/auteur/")
            
            source_auth = [a["href"] for a in source("a", href=uri_auth)]
            
            for a in source_auth:
                refAuthor = "https://pinakes.irht.cnrs.fr/{}".format(a)
                
                name_auth = source.find_all('a', attrs={'href': re.compile(r"^/notices/auteur/")})
                
                name_auth_list = []
                
                if name_auth:
                    for author in name_auth:
                        name_auth_list.append(author.get_text())
                        
        aaa = set(name_auth_list)
               
        for author in aaa:
            print(' '.join(title),delimiter,author,delimiter,placeName,file=open("../csv/mss_md_m03.csv", "a"))

