**Distant reading of manuscript data**  
DH Lab, Basel  
24th February 2020  
09:00-17:30  

## Exercice: *Using manuscript descriptions as data with XSLT*  

### Required tools  

* Oxygen XML Editor
* Excel / OfficeCalc
* [Palladio](https://hdlab.stanford.edu/palladio/) 

### Data Structure  

The files for the exercice are located in the folder `/exercice_XSLT`.  

The data are organised in sub-folders:  

* `/xml`: contains the corpus of XML input files  

* `/xsl`: contains the XSLT transformation files  

* `/csv`: contains the CSV output files  

* `/img`: contains the images showed in this document  


### Exercice steps  

#### 1. Get the xml input files      

The xsl files parse the data from the folder `/xml`.   

To start, just store the xml corpus inside the `/xml` folder. It does not matter if there are multiple folder and subfolders: the path to the files can be adjusted in the xsl files later. 

#### 2. Open the xsl file

The xsl files are located in the folder `/xsl`.  

Which xsl file for which output? There are three xsl files and each one produces a different output:  

* `mss_test_whole.xsl`: outputs the mss filename, title, identifier, material, language, date, date not before and date not after  
* `mss_material_to_century.xsl`: outputs the mss filename, material and century  
* `mss_title_to_person.xsl`: outputs the mss filename, title(s) and person(s)

To know which xml element correspond to each of these information, please refer to the `PARAMETERS` section in the xsl files.

To start the exercice, open the xsl file for the exercice in Oxygen.  

Some errors might appear - this is because the structure of the files does not correspond to the standard configuration. The errors will disappear once we set our own configuration (see next steps).  

#### 3. Select the corpus to parse

The corpus parsing instruction is located under the commented line `<!-- Parse all the xml files in the selected folder -->`.  

<img src="img/img_05.png", width="650">  

To parse the xml corpus, just set the corresponding path between the the quotation marks of the `collection(' ')` function.   

Use the following path syntax:

* `../xml` means *from the current folder, step back of one folder and go into the /xml folder*  

* `/./` means *get into the direct subfolders*  

* `.?select=*.xml` means *select all the files whose name ends with .xml*  

* `;recurse=yes` means *if there are multiple levels of subfolders, repeate the operation into all the subfolder*  

For an exemple, if the xml files are located into the folder `/xml/Barocci/Barocci_15th`, the path will look like this:

`'../xml/Barocci/Barocci_15th/.?select=*.xml'`  

If the folder `Barocci_15th` contains multiple subfolders, like `Barocci_15th_01`, `Barocci_15th_02`, `Barocci_15th_03`, etc., then you will need to add a `/./` for each level of subfolders, plus `;recurse=yes` :  

`'../xml/Barocci/Barocci_15th/./.?select=*.xml;recurse=yes'`  

#### 4. Configure the xsl transformation  

Once the path to the files is set, click on `Configure Transformation Scenario`.  

<img src="img/img_01.png", width="650">  

In the `Configure Transformation Scenario` window:  

* click on `New`  

* click on `XSLT Transformation`  

In the `New Scenario` window:
  
   * in the `XSLT` tab:  

    * on the right of the `XML URL` field, click on the yellow folder and navigate up to the `/xml` folder, then click on a random file (regardless of the file you select, the transformation will apply to the files in the path we set above) 

    * in the `XSL URL` field, leave the standard path to the current file : `${currentFileURL}`  

    * in the `Transformer` field, select the option `Saxon-PE 9.8.0.12` (this is what makes the errors disappear)
    
    The XSLT tab will look like this:  

<img src="img/img_02.png", width="650">  

   * in the `Output` tab:  

    * click on `Save As`, then click on the yellow folder on the right and navigate up to the `/csv` folder  
    
    * in the `/csv` folder, click on the csv file that has the same name as the xsl file. If a window shows up and asks if you want to overwrite it, click on `Yes`  
  
    * in `Show in results view as`, select `XML`  
    
    The `Output` tab will look like this:

<img src="img/img_03.png", width="650">  

   * click on `Ok`  

Back in the in the `Configure Transformation Scenario` window, click on `Apply Associated (1)`  

The results appear at the bottom of the page  

<img src="img/img_06.png", width="650">  


#### 5. Run the transformation  

The transformation should run once you click on `Apply Associated (1)` (see step above). 

To run it again, click on `Apply Transformation Scenario`

<img src="img/img_04.png", width="650">  


#### 6. Check the CSV output

The CSV output files are stored in the `/csv` folder.  

Each xsl file outputs one CSV file with the same name: i.e., the `mss_material_to_century.xsl` file outputs the `mss_material_to_century.csv` file.  

To check the CSV output of the transformation, go to the `/csv` folder and click on the file that resulted from the transformation (same name).  

Before the file opens, a window with some paramters should appear. There are two important parameters to set:  

* `Character set` as `Unicode (UTF8)` (allows the correct display of Greek characters)  

* `Separator Options` as `Semicolon` (the xsl file set the semicolon as field delimiter)

<img src="img/img_07.png", width="650">  

The CSV file opens  

<img src="img/img_08.png", width="650">  

#### 7. Process the output data in other tools

The output csv files can now be used in tools that process CSV data, such as Palladio or Google Maps.  

It might be required to change the type of delimiter and set it as a comma. To do this, go back to the xsl file and check the variable named `delimiter` (immediately below the namespace declaration).  

The `select` attribute of the variable has a semicolon a a value. If you need the delimiter to be a comma, just replace the semicolon with a comma.

<img src="img/img_09.png", width="650">  






