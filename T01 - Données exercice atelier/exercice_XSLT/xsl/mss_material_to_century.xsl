<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://tempuri.org/msxsl" xmlns:ns1="http://www.tei-c.org/ns/1.0">
    <xsl:output method="text" indent="yes" encoding="UTF-8"/>

    <!-- Strip the white spaces within the element tree -->
    <xsl:strip-space elements="*"/>

    <!-- ################################# VARIABLES ################################# -->

    <!-- Create a variable that sets the value of the delimiter -->
    <xsl:variable name="delimiter" select="';'"/>

    <!-- ################################# PARAMETERS ################################# -->

    <!-- PARAMETER FOR MSS FILENAME -->
    <xsl:template name="file_temp">

        <!-- Stock in a variable the name of the file from which the data are extracted -->
        <xsl:variable name="tokenized">
            <xsl:value-of select="tokenize(base-uri(), '/')[last()]"/>
        </xsl:variable>

        <!-- Output the name of the file from which the data are extracted -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted value and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($tokenized, $delimiter))"/>

    </xsl:template>

    <!-- PARAMETER FOR MSS MATERIAL -->
    <xsl:template name="material_temp">

        <!-- Create a parameter that extracts the value of the @material attribute within the <support> element -->
        <xsl:param name="material_par"
            select="//ns1:msDesc//ns1:physDesc//ns1:objectDesc//ns1:supportDesc//@material"/>

        <!-- Output the value of the @material attribute within the <support> element  -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($material_par, $delimiter))"/>
    </xsl:template>

    <!-- PARAMETER FOR DATE FROM CENTURY -->
    <xsl:template name="century_temp">
        <!-- Create a parameter that extracts the value of the @notBefore OR the @when attribute -->
        <xsl:param name="century_par"
            select="//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when"/>

        <!-- Create a list of conditions: if the value of the @notBefore OR the @when attribute starts with the given couple of digits (06, 07, 08, etc.)... -->
        <!-- ...then replace the value with the givent text (7th, 8th, 9th, etc.) -->

        <!-- Condition for 7th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^06.*')">
            <xsl:text>7th</xsl:text>
        </xsl:if>

        <!-- Condition for 8th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^07.*')">
            <xsl:text>8th</xsl:text>
        </xsl:if>

        <!-- Condition for 9th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^08.*')">
            <xsl:text>9th</xsl:text>
        </xsl:if>

        <!-- Condition for 10th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^09.*')">
            <xsl:text>10th</xsl:text>
        </xsl:if>

        <!-- Condition for 11th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^10.*')">
            <xsl:text>11th</xsl:text>
        </xsl:if>

        <!-- Condition for 12th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^11.*')">
            <xsl:text>12th</xsl:text>
        </xsl:if>

        <!-- Condition for 13h century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^12.*')">
            <xsl:text>13th</xsl:text>
        </xsl:if>

        <!-- Condition for 14th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^13.*')">
            <xsl:text>14th</xsl:text>
        </xsl:if>

        <!-- Condition for 15th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^14.*')">
            <xsl:text>15th</xsl:text>
        </xsl:if>

        <!-- Condition for 16th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^15.*')">
            <xsl:text>16th</xsl:text>
        </xsl:if>

        <!-- Condition for 17th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^16.*')">
            <xsl:text>17th</xsl:text>
        </xsl:if>

        <!-- Condition for 18th century -->
        <xsl:if
            test="matches(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore | //@when, '^17.*')">
            <xsl:text>18th</xsl:text>
        </xsl:if>

        <!-- Output the output of the replacement of each value (7th, 8th, 9th, etc.) -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($century_par, $delimiter))"/>

    </xsl:template>


    <!-- ################################# TEMPLATES ################################# -->

    <!-- The following template applies the list of parameters above -->
    <!-- and generates the output data structure in CSV format -->
    <xsl:template name="xml_to_csv" match="/">

        <!-- The HTML structure allows to create a table -->
        <!-- where extracted data are organised in rows and columns -->
        <html>

            <!-- Create a table -->
            <table>

                <!-- Create the header row -->
                <!-- The delimiter is retrieved from a variable, so that the value of the $delimiter variable can be modified, -->
                <!-- and the modification be automatically applied to each occurrence of the $delimiter variable in the file -->
                <tr>
                    <th>FILENAME<xsl:value-of select="$delimiter"/></th>
                    <th>MATERIAL<xsl:value-of select="$delimiter"/></th>
                    <th>CENTURY<xsl:value-of select="$delimiter"/></th>
                </tr>

                <!-- Create a new line after the header row -->
                <xsl:text>
</xsl:text>

                <!-- Parse all the xml files in the selected folder (INSERT PATH BETWEEN THE QUOTATION MARKS) -->
                <xsl:for-each select="collection(' ')">

                    <!-- Parse the <msDesc> elements in each file -->
                    <xsl:for-each select="//ns1:msDesc">

                        <!-- Create new rows -->
                        <!-- where each row contains metadata about one manuscript item -->
                        <tr>

                            <!-- Create a cell to contain the filename -->
                            <td>
                                <xsl:call-template name="file_temp">
                                    <xsl:with-param name="file_par" select="."/>
                                </xsl:call-template>
                            </td>

                            <!-- Create a cell to contain the mss material -->
                            <td>
                                <xsl:call-template name="material_temp">
                                    <xsl:with-param name="material_par"
                                        select="replace(//ns1:msDesc//ns1:physDesc//ns1:objectDesc//ns1:supportDesc//@material, ';', '.')"
                                    />
                                </xsl:call-template>
                            </td>

                            <!-- Create a cell to contain the mss century -->
                            <td>
                                <xsl:call-template name="century_temp">
                                    <xsl:with-param name="century_par"/>
                                </xsl:call-template>
                            </td>

                            <td> </td>

                            <!-- Add new line after each row (redundant though?) -->
                            <xsl:text>&#xa;</xsl:text>
                        </tr>



                    </xsl:for-each>

                </xsl:for-each>

            </table>

        </html>

    </xsl:template>

</xsl:stylesheet>
<!-- ################################# ADD NEW DATA ################################# -->
<!-- 1) Create a NEW HEADER CELL (<th>) -->
<!-- 2) Create a NEW PARAMETER (xsl:param) that selects the node from which the data are extracted -->
<!-- 3) Create a NEW CELL (<td>) that applies the parameter to extract data -->
<!-- ################################# APPLY TO OTHER DATASETS ################################# -->
<!-- 1) Change the PATH to the corpus -->
<!-- 2) Change the XPATHS to the elements of the tree structure-->
<!-- 3) Change the HEADER, ROWS and CELLS within the table -->
