<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://tempuri.org/msxsl" xmlns:ns1="http://www.tei-c.org/ns/1.0">
    <xsl:output method="text" indent="yes" encoding="UTF-8"/>

    <!-- Strip the white spaces within the element tree -->
    <xsl:strip-space elements="*"/>

    <!-- ################################# VARIABLES ################################# -->

    <!-- Create a variable that sets the value of the delimiter -->
    <xsl:variable name="delimiter" select="';'"/>

    <!-- ################################# PARAMETERS ################################# -->

    <!-- PARAMETER FOR MSS FILENAME -->
    <xsl:template name="file_temp">

        <!-- Stock in a variable the name of the file from which the data are extracted -->
        <xsl:variable name="tokenized">
            <xsl:value-of select="tokenize(base-uri(), '/')[last()]"/>
        </xsl:variable>

        <!-- Output the name of the file from which the data are extracted -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted value and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($tokenized, $delimiter))"/>

    </xsl:template>

    <!-- PARAMETER FOR MSS TITLE -->
    <xsl:template name="title_temp">

        <!-- Create a parameter that extracts the content of the <title> element -->
        <xsl:param name="title_par" select="//ns1:msContents//ns1:msItem//ns1:title"/>

        <!-- Output the content of the <title> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($title_par, $delimiter))"/>

    </xsl:template>

    <!-- PARAMETER FOR MSS PERSON -->
    <xsl:template name="person_temp">

        <!-- Create a parameter that extracts the value of the @ref attribute within the <title> element -->
        <xsl:param name="person_par"
            select="//ns1:msContents//ns1:msItem//ns1:author//ns1:name//@ref"/>

        <!-- Output the value of the @ref attribute within the <title> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($person_par, $delimiter))"/>

    </xsl:template>


    <!-- ################################# TEMPLATES ################################# -->

    <!-- The following template applies the list of parameters above -->
    <!-- and generates the output data structure in CSV format -->
    <xsl:template name="xml_to_csv" match="/">

        <!-- The HTML structure allows to create a table -->
        <!-- where extracted data are organised in rows and columns -->
        <html>

            <!-- Create a table -->
            <table>

                <!-- Create the header row -->
                <!-- The delimiter is retrieved from a variable, so that the value of the $delimiter variable can be modified, -->
                <!-- and the modification be automatically applied to each occurrence of the $delimiter variable in the file -->
                <tr>
                    <th>FILENAME<xsl:value-of select="$delimiter"/></th>
                    <th>TITLE<xsl:value-of select="$delimiter"/></th>
                    <!--<th>PERSON<xsl:value-of select="$delimiter"/></th>-->
                </tr>

                <!-- Create a new line after the header row -->
                <xsl:text>
</xsl:text>

                <!-- Parse all the xml files in the selected folder (INSERT PATH BETWEEN THE QUOTATION MARKS)-->
                <xsl:for-each select="collection(' ')">

                    <!-- Parse the <msDesc> element in each file -->
                    <xsl:for-each select="//ns1:msDesc">

                        <!-- Parse the <msItem> elements in each file -->
                        <xsl:for-each select="//ns1:msItem">

                            <!-- Create new rows -->
                            <!-- where each row contains metadata about one manuscript item -->
                            <tr>

                                <!-- Create a cell to contain the filename -->
                                <td>
                                    <xsl:call-template name="file_temp">
                                        <xsl:with-param name="file_par" select="."/>
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss title -->
                                <!-- Note: the replace() function replaces the semicolons in the text with a dot, -->
                                <!-- because semicolons are already used to mark delimitation between the cells -->
                                <td>
                                    <xsl:call-template name="title_temp">
                                        <xsl:with-param name="title_par"
                                            select="replace(ns1:title, ';', '.')"/>
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss person id -->
                                <!-- Note: the replace() function replaces the semicolons in the text with a dot, -->
                                <!-- because semicolons are already used to mark delimitation between the cells -->
                                <td>
                                    <xsl:call-template name="person_temp">
                                        <xsl:with-param name="person_par"
                                            select="replace(//ns1:author//ns1:name//@ref, ';', ',')"
                                        />
                                    </xsl:call-template>
                                </td>


                                <!-- Create a cell to contain the mss person name -->
                                <!-- Note: the replace() function replaces the semicolons in the text with a dot, -->
                                <!-- because semicolons are already used to mark delimitation between the cells -->
                                <!-- UNCOMMENT THE LINES BELOW TO RETRIEVE THE NAMES -->
                                <!--<td>
                                    <xsl:call-template name="person_temp">
                                        <xsl:with-param name="person_par"
                                            select="replace(//ns1:author//ns1:name//ns1:persName, ';', ',')"/>
                                    </xsl:call-template>
                                </td>-->


                                <!-- Add new line after each row (redundant though?) -->
                                <xsl:text>&#xa;</xsl:text>
                            </tr>

                        </xsl:for-each>

                    </xsl:for-each>

                </xsl:for-each>

            </table>

        </html>

    </xsl:template>

</xsl:stylesheet>
<!-- ################################# ADD NEW DATA ################################# -->
<!-- 1) Create a NEW HEADER CELL (<th>) -->
<!-- 2) Create a NEW PARAMETER (xsl:param) that selects the node from which the data are extracted -->
<!-- 3) Create a NEW CELL (<td>) that applies the parameter to extract data -->
<!-- ################################# APPLY TO OTHER DATASETS ################################# -->
<!-- 1) Change the PATH to the corpus -->
<!-- 2) Change the XPATHS to the elements of the tree structure-->
<!-- 3) Change the HEADER, ROWS and CELLS within the table -->
