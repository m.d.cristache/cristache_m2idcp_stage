<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:user="http://tempuri.org/msxsl" xmlns:ns1="http://www.tei-c.org/ns/1.0">
    <xsl:output method="text" indent="yes" encoding="UTF-8"/>

    <!-- Strip the white spaces within the element tree -->
    <xsl:strip-space elements="*"/>

    <!-- ################################# VARIABLES ################################# -->

    <!-- Create a variable that sets the value of the delimiter -->
    <xsl:variable name="delimiter" select="';'"/>

    <!-- ################################# PARAMETERS ################################# -->

    <!-- PARAMETER FOR MSS FILENAME -->
    <xsl:template name="file_temp">

        <!-- Stock in a variable the name of the file from which the data are extracted -->
        <xsl:variable name="tokenized">
            <xsl:value-of select="tokenize(base-uri(), '/')[last()]"/>
        </xsl:variable>

        <!-- Output the name of the file from which the data are extracted -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted value and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($tokenized, $delimiter))"/>

    </xsl:template>

    <!-- PARAMETER FOR MSS TITLE -->
    <xsl:template name="title_temp">

        <!-- Create a parameter that extracts the content of the <title> element -->
        <xsl:param name="title_par" select="//ns1:msContents//ns1:msItem//ns1:title"/>

        <!-- Output the content of the <title> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($title_par, $delimiter))"/>

    </xsl:template>

    <!-- PARAMETER FOR MSS ID -->
    <xsl:template name="id_temp">

        <!-- Create a parameter that extracts the value of the @key attribute within the <title> element -->
        <xsl:param name="id_par" select="//ns1:msContents//ns1:msItem//ns1:title//@key"/>

        <!-- Output the value of the @key attribute within the <title> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted value -->
        <!-- 'concat'  concatenates the extracted value and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($id_par, $delimiter))"/>
    </xsl:template>

    <!-- PARAMETER FOR MSS MATERIAL -->
    <xsl:template name="material_temp">

        <!-- Create a parameter that extracts the content of the <support> element -->
        <xsl:param name="material_par"
            select="//ns1:msDesc//ns1:physDesc//ns1:objectDesc//ns1:supportDesc//ns1:support"/>

        <!-- Output the content of the <support> element  -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($material_par, $delimiter))"/>
    </xsl:template>

    <!-- PARAMETER FOR MSS LANGUAGE -->
    <xsl:template name="language_temp">

        <!-- Create a parameter that extracts the content of the <textLang> element -->
        <xsl:param name="language_par" select="//ns1:msContents//ns1:msItem//ns1:textLang"/>

        <!-- Output the content of the <textLang> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($language_par, $delimiter))"/>
    </xsl:template>

    <!-- PARAMETER FOR MSS DATE -->
    <xsl:template name="date_temp">

        <!-- Create a parameter that extracts the content of the <origDate> element -->
        <xsl:param name="date_par"
            select="//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate"/>

        <!-- Output the content of the <origDate> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted contents -->
        <!-- 'concat'  concatenates the extracted contents and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($date_par, $delimiter))"/>

    </xsl:template>

    <!-- PARAMETER FOR MSS NOT-BEFORE DATE -->
    <xsl:template name="not_before_temp">

        <!-- Create a parameter that extracts the value of the @notBefore attribute within the <origDate> element -->
        <xsl:param name="not_before_par"
            select="//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore"/>

        <!-- Output the value of the @notBefore attribute within the <origDate> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted value -->
        <!-- 'concat'  concatenates the extracted value and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($not_before_par, $delimiter))"/>

    </xsl:template>

    <!-- PARAMETER FOR MSS NOT-AFTER DATE -->
    <xsl:template name="not_after_temp">

        <!-- Create a parameter that extracts the value of the @notAfter attribute within the <origDate> element -->
        <xsl:param name="not_after_par"
            select="//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notAfter"/>

        <!-- Output the value of the @notAfter attribute within the <origDate> element -->
        <!-- 'normalize-space' strips blank spaces and new lines from the extracted value -->
        <!-- 'concat'  concatenates the extracted value and the CSV delimiter -->
        <xsl:value-of select="normalize-space(concat($not_after_par, $delimiter))"/>

    </xsl:template>

    <!-- ################################# TEMPLATES ################################# -->

    <!-- The following template applies the list of parameters above -->
    <!-- and generates the output data structure in CSV format -->
    <xsl:template name="xml_to_csv" match="/">

        <!-- The HTML structure allows to create a table -->
        <!-- where extracted data are organised in rows and columns -->
        <html>

            <!-- Create a table -->
            <table>

                <!-- Create the header row -->
                <!-- The delimiter is retrieved from a variable, so that the value of the $delimiter variable can be modified, -->
                <!-- and the modification be automatically applied to each occurrence of the $delimiter variable in the file -->
                <tr>
                    <th>FILENAME<xsl:value-of select="$delimiter"/></th>
                    <th>TITLE<xsl:value-of select="$delimiter"/></th>
                    <th>ID<xsl:value-of select="$delimiter"/></th>
                    <th>MATERIAL<xsl:value-of select="$delimiter"/></th>
                    <th>LANGUAGE<xsl:value-of select="$delimiter"/></th>
                    <th>DATE<xsl:value-of select="$delimiter"/></th>
                    <th>DATE not after<xsl:value-of select="$delimiter"/></th>
                    <th>DATE not before<xsl:value-of select="$delimiter"/></th>
                </tr>

                <!-- Create a new line after the header row -->
                <xsl:text>
</xsl:text>

                <!-- Parse all the xml files in the selected folder (INSERT PATH BETWEEN THE QUOTATION MARKS)-->
                <xsl:for-each select="collection(' ')">

                    <!-- Parse the <msDesc> element in each file -->
                    <xsl:for-each select="//ns1:msDesc">

                        <!-- Parse the <msItem> elements in each file -->
                        <xsl:for-each select="//ns1:msItem">

                            <!-- Create new rows -->
                            <!-- where each row contains metadata about one manuscript item -->
                            <tr>

                                <!-- Create a cell to contain the filename -->
                                <td>
                                    <xsl:call-template name="file_temp">
                                        <xsl:with-param name="file_par" select="."/>
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss title -->
                                <!-- Note: the replace() function replaces the semicolons in the text with a dot, -->
                                <!-- because semicolons are already used to mark delimitation between the cells -->
                                <td>
                                    <xsl:call-template name="title_temp">
                                        <xsl:with-param name="title_par"
                                            select="replace(ns1:title, ';', '.')"/>
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss identifier -->
                                <td>
                                    <xsl:call-template name="id_temp">
                                        <xsl:with-param name="id_par"
                                            select="replace(ns1:title//@key, ';', '.')"/>
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss material -->
                                <td>
                                    <xsl:call-template name="material_temp">
                                        <xsl:with-param name="material_par"
                                            select="replace(//ns1:msDesc//ns1:physDesc//ns1:objectDesc//ns1:supportDesc//@material, ';', '.')"
                                        />
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss language -->
                                <td>
                                    <xsl:call-template name="language_temp">
                                        <xsl:with-param name="language_par"
                                            select="replace(ns1:textLang, ';', '.')"/>
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss date -->
                                <td>
                                    <xsl:call-template name="date_temp">
                                        <xsl:with-param name="date_par"
                                            select="replace(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate, ';', '.')"
                                        />
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss not-before date -->
                                <td>
                                    <xsl:call-template name="not_before_temp">
                                        <xsl:with-param name="not_before_par"
                                            select="replace(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notBefore, ';', '.')"
                                        />
                                    </xsl:call-template>
                                </td>

                                <!-- Create a cell to contain the mss not-after date -->
                                <td>
                                    <xsl:call-template name="not_after_temp">
                                        <xsl:with-param name="not_after_par"
                                            select="replace(//ns1:sourceDesc//ns1:msDesc//ns1:history//ns1:origin//ns1:origDate//@notAfter, ';', '.')"
                                        />
                                    </xsl:call-template>
                                </td>

                                <!-- Add new line after each row (redundant though?) -->
                                <xsl:text>&#xa;</xsl:text>
                            </tr>

                        </xsl:for-each>

                    </xsl:for-each>

                </xsl:for-each>

            </table>

        </html>

    </xsl:template>

</xsl:stylesheet>
<!-- ################################# ADD NEW DATA ################################# -->
<!-- 1) Create a NEW HEADER CELL (<th>) -->
<!-- 2) Create a NEW PARAMETER (xsl:param) that selects the node from which the data are extracted -->
<!-- 3) Create a NEW CELL (<td>) that applies the parameter to extract data -->
<!-- ################################# APPLY TO OTHER DATASETS ################################# -->
<!-- 1) Change the PATH to the corpus -->
<!-- 2) Change the XPATHS to the elements of the tree structure-->
<!-- 3) Change the HEADER, ROWS and CELLS within the table -->
