#!/usr/bin/env python
# coding: utf-8

# In[14]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Developing adaptative computational models for research-oriented outputs to mss metadata.

MODEL N°1
Extracts the names and identifiers of places in TEI metadata files, and 
retrieves their latitude and longitude from the matching Getty Geonames rdf description.
The results are output in CSV format and suit the requirements of Google Earth Pro. 

Created on February 2020.
"""

#-------------------------------------------------------------------------------------------

# The BeautifulSoup library parses the element trees of the XML input files
from bs4 import BeautifulSoup

# The re module provides regular expressions matching operations
import re

# The glob module generates lists of files matching given patterns (file paths)
import glob

# The pathlib module offers a set of classes to handle filesystem paths
from pathlib import Path

# The rdflib library allows working with RDF
import rdflib

#-------------------------------------------------------------------------------------------

__author__ = "Diandra Cristache"
__copyright__ = " "
__credits__ = ["Cambridge Digital Library"]
__license__ = "Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) "
__maintainer__ = "Diandra Cristache"
__email__ = "diandra.cristache@etu.univ-tours.fr"
__status__ = "Production"

#-------------------------------------------------------------------------------------------

# Set the CSV delimiter (according to the requirement of the output tool)
# and store it in the variable 'delimiter'
delimiter = ","
                    
# Set the header row (separate the fields with a delimiter)
# Print the header row in the output CSV file
print("TITLE",delimiter,"PLACE",delimiter,"LAT",delimiter,"LON", file=open("../csv/output.csv", "w"))

# Select the path of the corpus to parse
# and store it in the variable 'corpus'
corpus = glob.glob('../xml/*/*.xml')

# Parse each file in the corpus
for file in corpus:

    # Open the original file 
    # and store the reference in the variable 'file_open'
    file_open = open(file)

    # Read the file content 
    # and store it in the variable 'file_content'
    file_content = file_open.read()

    # Give BeautifulSoup access to the file content 
    # and store the access in the variable 'soup'
    soup = BeautifulSoup(file_content, "xml")

    # Close the original file
    file_open.close()
        
    # Parse each file of the corpus, while it is open in reading mode
    with open(file, 'r', encoding="utf-8") as content:
        
        # Find all the occurrences of the element <material> 
        # which have an attribute @type
        # and store the results in the list 'mssMaterial'
        # [uncomment the line below to run it]
        #mssMaterial = soup.select('material', {'type':re.compile(r'.*')})
        
        # Find all the occurrences of the element <title> 
        # (only when sub-elements of <titleStmt>)
        # and store the results in the list 'mssTitle'
        mssTitle = soup.select('titleStmt title')

        # For each element <title> in the list
        for a in mssTitle:

            # Get the content of the element <title>
            # Store the content in the variable 'title'
            # and strip the useless blank spaces and new lines
            title = a.get_text().split()

            # Find all the occurrences of the element <origPlace>
            # which have an attribute @ref
            # and store the results in the list 'placeList'
            placeList = soup.find_all('origPlace', {'ref':re.compile(r'.*')})

            # For each element <origPlace> in the list
            for place in placeList:

                # Retrieve the content of the element <origPlace>
                # replace all the commas with another character (the comma is already in use as a delimiter)
                # and strip the useless blank spaces and new lines
                placeName = place.get_text().replace(',', ' -').split()

                # Retrieve the value of the @ref attribute (the unique identifier of the place)
                placeKey = str(place['ref'])

                # Insert the value of the @ref attribute
                # into the standard URI of the Getty rdf description
                # and store the URI into the variable 'uri'
                uri = "http://vocab.getty.edu/tgn/{}.rdf".format(placeKey)

                # Call the rdflib library
                g = rdflib.Graph()

                try:

                    # (For each element <origPlace> in the list) parse the URI 
                    parse = g.parse(uri)
                    
                    # ---------------- GET THE LATITUDE ----------------

                    # Launch a SPARQL query that finds the element <schema:latitude>
                    # from the matching Getty Geonames rdf description
                    qres_lat = parse.query(
                        """SELECT ?latitude
                           WHERE {
                              ?x schema:latitude ?latitude .
                           }""")
                    
                    # For each result of the query
                    for row_lat in qres_lat:

                        # Store the content of <schema:latitude> in the variable 'row_lat'
                        row_lat = "%s" %row_lat
                        
                    # ---------------- GET THE LONGITUDE ----------------
                    
                    # Launch a SPARQL query that finds the element <schema:longitude>
                    # from the matching Getty Geonames rdf description
                    qres_lon = parse.query(
                        """SELECT ?longitude
                           WHERE {
                              ?x schema:longitude ?longitude .
                           }""")
                        
                    # For each result of the query
                    for row_lon in qres_lon:

                        # Store the content of <schema:longitude> in the variable'row_lon'
                        row_lon = "%s" %row_lon

                # If the file is not found (the URI does not exist, or is not well formed)
                except FileNotFoundError:

                    # Ignore the error and continue parsing
                    pass
            
            # For each file in the corpus, print the extracted information in a row
            # and append the row in the unique CSV output file
            print(' '.join(title),delimiter,' '.join(placeName),delimiter,row_lat,delimiter,row_lon, file=open("../csv/output.csv", "a"))


