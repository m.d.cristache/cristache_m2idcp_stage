**Distant reading of manuscript data**  
DH Lab, Basel  
24th February 2020  
09:00-17:30  

## Exercice: *Using manuscript descriptions as data with Python*  

### Required tools  

* Oxygen XML Editor  
* Excel / OfficeCalc  
* a python launcher  
* [geojson](http://geojson.io/#map=2/20.0/0.0) and/or [Google Earth Pro](https://www.google.com/earth/versions/)

### Data Structure  

The files for the exercice are located in the folder `/exercice_PYTHON`.  

The data are organised in sub-folders:  

* `/py` : contains the Python scripts for the exercice

* `/xml`: contains the corpus of XML input files  

* `/csv`: contains the CSV output files  

* `/img`: contains the images showed in this document  

### Exercice steps  

#### 1. Get the xml input files      

The scripts parse the data from the folder `/xml`.   

To start, just store the xml corpus inside the `/xml` folder. It does not matter if there are multiple folder and subfolders: the path to the files can be adjusted in the scripts later. 

#### 2. Open the script file

The python scripts are located in the folder `/py`.  

The scripts are all named with a shortened form for *manuscripts* (mss) *metadata* (md) *model* (m) *number* (n°) and the number of the model. Example: mss_md_m01.py .

Which script for which output? There are * scripts and each one produces a different output:  

* `mss_md_m01.py` : outputs the mss title, place of origin, latitude and longitude of the place of origin  

* ...

Each script exists in two different file formats:  

* the `.py` format can be run from any Python launcher  

* the `.ipynb` format can be run in the Jupyter notebook environment  

If you do not use a Jupyter notebook, just open the script in .py format (any code editor will do). 

#### 3. Select the corpus to parse

The corpus parsing instruction is located under the commented line `# Select the path of the corpus to parse` (around line 54).  

<img src="img/img_01.png", width="650">  

To parse the xml corpus, just set the corresponding path between the quotation marks of the `glob.glob(' ')` command.  

Use the following path syntax:

* `../xml` means *from the current folder, step back of one folder and go into the /xml folder*  

* `/*/` means *get into all the direct subfolders*  

* `*.xml` means *select all the files whose name ends with .xml*  

For an exemple, if the xml files are located into the folder `/xml`, and if the folder `/xml` contains one level of subfolders, the path will look like this:  

`'../xml/*/*.xml'`

#### 4. Run the Python script  

The Python script is located in the folder `/py`.  

For a reminder, the folder `/py` contains the same script in two different file formats:  

* the `.py` format can be run from any Python launcher  

* the `.ipynb` format can be run in the Jupyter notebook environment  

If you do not use a Jupyter notebook, just run the script in .py format. 

#### 5. Check the CSV output  

The output file is stored in the `/csv` folder.  

#### 6. Import the output data in tools for geographic visualization

##### a. Geojson

##### b. Google Earth Pro   

Launch Google Earth Pro.  

<img src="img/img_02.png", width="650">  

In `File` (top left corner) click on `Import`.

<img src="img/img_03.png", width="650">  

Click on the output file `mss_md_m01.csv`.

<img src="img/img_04.png", width="650">  

Set the following options in the `Data Import Wizard`:  

* `Field Type` is `Delimited`  

* `Delimited` is `Comma` 

* `Text Encoding` is `UTF-8`

Click on `Next`.

<img src="img/img_05.png", width="650">  

Select the latitude and longitude fields:  

* `Latitude Field` is `LAT`  

* `Longitude Field` is `LON`  

Click on `Next`.

<img src="img/img_06.png", width="650">  

You can also specify the field types:

* `TITLE` would be `string`  
* `PLACE` would be `string`  
* `LAT` would be `floating point`  
* `LON` would be `floating point`  

Click on `Finish`.

<img src="img/img_07.png", width="650">  

You can also apply a style template to the data. Click on `Yes`.

<img src="img/img_08.png", width="650">  

The window `Style Template Settings` contains four tabs.  

The tab `Name` defines the label of the points on the map.  

Select the column whose content you want to define as a label.

<img src="img/img_09.png", width="650">  

The tab `color` defines the color of the points on the map.  

<img src="img/img_10.png", width="650">  

The tab `icon` defines the icon for the points on the map.  

<img src="img/img_11.png", width="650">  

The tab `Height` defines the height of the points from the surface of the field.

<img src="img/img_12.png", width="650">  

Once the options are set, click on `Ok`. 

To save the template, click on `Save`.

<img src="img/img_13.png", width="650">  

The points might not appear on the map yet. Look at the tab `Places` in the sidebar: the name of the imported CSV file (here, `mss_md_m01.csv`) is probably unchecked. 

<img src="img/img_14.png", width="650">  

Check the name of the imported CSV file.

<img src="img/img_15.png", width="650">  

The geographic points from the CSV files now appear on the map.

<img src="img/img_16.png", width="650">  


